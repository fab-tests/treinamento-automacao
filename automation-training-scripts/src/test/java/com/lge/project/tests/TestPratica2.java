package com.lge.project.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestPratica2 {

	public WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	@After
	public void after() throws Exception {
		driver.close();
	}

	@Test
	public void testPratica2() {
		boolean isEquals = false;
		driver.get("http://136.166.96.146:8080/SimpleWebApp/");
		//driver.findElement(By.cssSelector("#mainPage > ul > li:nth-child(5) > a")).click();
		driver.findElement(By.linkText("Create Product")).click();
		String text = driver.findElement(By.xpath("//*[@id=\"mainPage\"]/div/div[1]/h2")).getText();
		driver.findElement(By.cssSelector("#code")).sendKeys("27");
		driver.findElement(By.id("name")).sendKeys("NovoProduto");
		driver.findElement(By.xpath("//*[@id=\"price\"]")).sendKeys("10");
		driver.findElement(By.xpath("//*[@id=\"mainPage\"]/form/button")).click();

		
		WebElement tbody = driver.findElement(By.xpath("//*[@id=\"mainPage\"]/table/tbody"));
		List<WebElement> allRowsTable = tbody.findElements(By.tagName("tr"));
		for (int i = 2; i <= allRowsTable.size() && !isEquals; i++) {
			 isEquals = "NovoProduto".equals(allRowsTable.get(i)
					.findElement(By.xpath("//*[@id=\"mainPage\"]/table/tbody/tr[" + i + "]/td[2]")).getText());
		}
		assertEquals("Create Product", text);
		assertTrue(isEquals);
	}
}