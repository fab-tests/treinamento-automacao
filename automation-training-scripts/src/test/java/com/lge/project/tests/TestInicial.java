package com.lge.project.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestInicial {

	public WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@After
	public void after() throws Exception {
		driver.close();
	}

	@Test
	public void testPratica1() {
		driver.get("http://136.166.96.146:8080/SimpleWebApp/");
		driver.findElement(By.xpath("//*[@id=\"mainPage\"]/ul/li[2]/a")).click();
		String text = driver.findElement(By.xpath("//*[@id=\"mainPage\"]/div/div[1]/h2")).getText();
		WebElement tbody = driver.findElement(By.xpath("//*[@id=\"mainPage\"]/table/tbody"));
		List<WebElement> allRowsTable = tbody.findElements(By.tagName("tr"));
		int size = allRowsTable.size() - 1;
		String text2 = driver.findElement(By.xpath("//*[@id=\"mainPage\"]/p[2]")).getText();
		String qtdeRows = text2.split(": ")[1];

		assertEquals("Product List", text);
		assertEquals(String.valueOf(size), qtdeRows);
	}

}